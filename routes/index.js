var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/1', function(req, res) {
  res.render('index', { title: 'Umpfel' });
});

router.get('/2', function(req, res) {
  res.render('cover', { title: 'Umpfel' });
});
router.get('/3', function(req, res) {
  res.render('carousel', { title: 'Umpfel' });
});

module.exports = router;
